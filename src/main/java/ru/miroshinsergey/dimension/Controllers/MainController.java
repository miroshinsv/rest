package ru.miroshinsergey.dimension.Controllers;


import org.json.JSONObject;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;

import org.springframework.web.bind.annotation.*;

@RestController
public class MainController {

    @PostMapping(value = "/task", produces = "application/json")
    public String getRequestBody(@RequestBody String body){
        JSONObject map = new JSONObject(String.format("{%s}",body));
        if ((int) map.get("id") == 76699046) {
            return "{\"id\":76699046, \"name\": \"Task 1\",\"mark\": 3.5}";
        } else {
            return "{\"code\": 404, \"description\": \"Not Found\"}";
        }
    }

    @ResponseStatus(HttpStatus.OK)
    @ExceptionHandler(value = HttpMessageNotReadableException.class)
    public String handleIOException() {
        return "{\"code\": 400, \"description\": \"Bad Request\"}";
    }
}
