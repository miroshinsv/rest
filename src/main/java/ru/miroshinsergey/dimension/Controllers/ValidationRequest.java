package ru.miroshinsergey.dimension.Controllers;

import javax.validation.constraints.NotBlank;

public class ValidationRequest {
    @NotBlank(message = "{\"code\": 404, \"description\": \"Not Found\"}")
    String notFound;

    @NotBlank(message = "{\"code\": 400, \"description\":\t\"Bad Request\"}")
    String badRequest;

    public String getNotFound() {
        return notFound;
    }

    public String getBadRequest() {
        return badRequest;
    }
}
