package ru.miroshinsergey.dimension;

import com.jayway.restassured.RestAssured;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;

public class Test02Negative {
    @BeforeMethod
    void setUp(){
        RestAssured.baseURI = "http://localhost:8080";
        RestAssured.basePath = "/task";
    }

    @Test(description = "Тест на проверку несуществующей задачи")
    void test01(){
        RestAssured
                .given()
                .body("\"id\":123")
                .post()
                .then()
                .assertThat()
                .log().all()
                .statusCode(200)
                .body("code",is(404))
                .body("description",containsString("Not Found"))
        ;
    }

    @Test(description = "Тест на проверку невалидных данных")
    void test02(){
        RestAssured
                .post()
                .then()
                .assertThat()
                .log().all()
                .statusCode(200)
                .body("code",is(400))
                .body("description",containsString("Bad Request"))
        ;
    }
}
