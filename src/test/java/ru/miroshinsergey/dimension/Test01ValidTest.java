package ru.miroshinsergey.dimension;

import com.jayway.restassured.RestAssured;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.hamcrest.CoreMatchers.*;

public class Test01ValidTest {
    @BeforeMethod
    void setUp(){
        RestAssured.baseURI = "http://localhost:8080";
        RestAssured.basePath = "/task";
    }

    @Test(description = "Тест с валидными данными")
    void test01(){
        RestAssured
                .given()
                .body("\"id\":76699046")
                .post()
                .then()
                .assertThat()
                .statusCode(200)
                .body("id",is(76699046))
                .body("name",is("Task 1"))
                .body("mark",equalTo(3.5f))
        ;
    }
}
